let
  CF = ../deps/Containerfile.dhall

let
  setEnv: Text -> CF.Type =
    \(path: Text) ->
      CF.env (toMap { CABAL = path })

let
  installFromBindist: Text -> CF.Type =
    \(bindistUrl: Text) ->
      let
        path: Text = "/usr/local/bin/cabal"
      in
        CF.run "install cabal"
        [ "curl -L -O ${bindistUrl}"
        , "mkdir -p cabal-unpack"
        , "tar xf cabal-*.tar.* -C cabal-unpack/"
        , "mv cabal-unpack/cabal ${path}"
        , "rm -rf cabal-unpack"
        , "${path} --version"
        ]
      # setEnv path

let 
  installFromSource: Text -> CF.Type =
    \(version: Text) ->
      let
        path: Text = "/usr/local/bin"
      let
        storeDir: Text = "/opt/cabal/store"
      in
        CF.run "install cabal"
        [ "cabal update"
        , "mkdir -p ${storeDir}"
        , "cabal install --store-dir=${storeDir}/store --enable-static --install-method=copy --installdir=${path} cabal-install==${version}"
        , "${path} --version"
        , "rm -rf ${storeDir}"
        ]
      # setEnv path

let
  type: Type =
    < FromBindist : Text
    | FromSource : Text
    | FromDistribution : Text
    >

let fromUpstreamBindist = 
  \(opts: { triple: Text, version: Text }) ->
      type.FromBindist "https://downloads.haskell.org/cabal/cabal-install-${opts.version}/cabal-install-${opts.version}-${opts.triple}.tar.xz"

let
  install: type -> CF.Type =
    \(src: type) ->
        merge
        { FromBindist = installFromBindist
        , FromSource = installFromSource
        , FromDistribution = setEnv
        } src

in
{ Type = type
, install = install
, fromUpstreamBindist = fromUpstreamBindist
}
