let
  Prelude = ../deps/Prelude.dhall
let
  CF = ../deps/Containerfile.dhall

let
  outDir: Text = "/opt/toolchain"
let
  binDir: Text = "${outDir}/bin"
let
  hackageIndexState: Text = "2023-09-18T18:43:12Z"

let
  cabalInstall: List Text -> CF.Type = \(args : List Text) ->
    CF.run "build haskell tools"
    [ "mkdir -p ${outDir}/store"
    , "$CABAL user-config update"
       -- We must override store-dir lest data files end up in
       -- /root/.cabal, which isn't accessible to the build user
    , ''
      $CABAL \
        --store-dir=${outDir}/store \
        v2-install \
        --constraint='alex ^>= 3.2.7.3' \
        --constraint='happy ^>= 1.20' \
        --with-compiler=$GHC \
        --enable-static \
        --install-method=copy \
        --installdir=${binDir} \
        --index-state=${hackageIndexState} \
        '' ++ Prelude.Text.concatSep " " args
    ]

let
  installGhcBuildDeps: CF.Type =
    cabalInstall [ "hscolour", "happy", "alex" ]
    # CF.env (toMap
      { HAPPY = "${binDir}/happy"
      , ALEX = "${binDir}/alex"
      , HSCOLOUR = "${binDir}/HsColour"
      })

let
  installHLint: CF.Type =
    cabalInstall [ "hlint-3.5" ]
    # CF.env (toMap
      { HLINT = "${binDir}/hlint"
      })

in
{
    installGhcBuildDeps = installGhcBuildDeps,
    installHLint = installHLint
}
