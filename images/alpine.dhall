-- Alpine Linux Docker images

let
  Prelude = ../deps/Prelude.dhall
let
  CF = ../deps/Containerfile.dhall
let
  HaskellTools = ../components/HaskellTools.dhall
let
  Cabal = ../components/Cabal.dhall
let
  Ghc = ../components/Ghc.dhall
let
  Image = ../Image.dhall

let
  docker_base_url: Text = "registry.gitlab.haskell.org/ghc/ci-images"

let
  coreBuildDepends: List Text =
    [ "alpine-sdk"
    , "autoconf"
    , "automake"
    , "bash"
    , "binutils-gold"
    , "build-base"
    , "coreutils"
    , "cpio"
    , "curl"
    , "gcc"
    , "git"
    , "gmp"
    , "gmp-dev"
    , "grep"
    , "libffi"
    , "linux-headers"
    , "gzip"
    , "jq"
    , "libffi-dev"
    , "lld"
    , "musl-dev"
    , "musl-locales"
    , "ncurses-dev"
    , "ncurses-libs"
    , "ncurses-static"
    , "perl"
    , "python3"
    , "sudo"
    , "unzip"
    , "wget"
    , "xz"
    , "zlib-dev"
    ]

let
  docsBuildDepends: List Text =
    [ "py3-sphinx"
    , "texlive"
    , "texlive-xetex"
    , "texmf-dist-latexextra"
    , "ttf-dejavu"
    ]

let installPkgs: List Text -> CF.Type = \(pkgs: List Text) ->
      CF.run "Installing packages: ${Prelude.Text.concatSep " " pkgs}"
      [ "apk add --no-cache ${Prelude.Text.concatSep " " pkgs}" ]

let installGhcDepsStep: CF.Type =
      installPkgs (coreBuildDepends # docsBuildDepends)

let installWasmDepsStep: CF.Type =
      CF.run "Installing wasm backend dependencies"
      [ "curl -f -L --retry 5 https://gitlab.haskell.org/ghc/ghc-wasm-meta/-/archive/3c264bbecc927fd09eed3229f8f89efbbcb5db6a/ghc-wasm-meta-3c264bbecc927fd09eed3229f8f89efbbcb5db6a.tar.gz | tar xz"
      , "cd ghc-wasm-meta-*"
      , "SKIP_GHC=1 ./setup.sh"
      , "cd .."
      , "rm -r ghc-wasm-meta-*" ]

let
  ghcVersion: Text = "9.4.3"

let
  fetchGhcStep: Ghc.Bindist -> CF.Type =
    \(ghcBindist: Ghc.Bindist) ->
      let
        destDir: Text = "/opt/ghc/${ghcVersion}"
      in
        Ghc.install
          { bindist = ghcBindist
          , destDir = destDir
          , configureOpts = ["--disable-ld-override"] : List Text
          }
        # CF.env (toMap { GHC = "${destDir}/bin/ghc" })

-- Create a normal user
let
  createUserStep: CF.Type =
      CF.run "create user"
      [ "adduser ghc --gecos 'GHC builds' --disabled-password"
      , "echo 'ghc ALL = NOPASSWD : ALL' > /etc/sudoers.d/ghc"
      ]
    # [CF.Statement.User "ghc"]
    # CF.workdir "/home/ghc/"
    # CF.run "update cabal index" [ "$CABAL update"]

let
  images: List Image.Type =
  [ Image ::
    { name = "x86_64-linux-alpine3_12"
    , runnerTags = [ "x86_64-linux" ]
    , image =
        CF.from "alpine:3.12.12"
      # [ CF.Statement.Shell ["/bin/ash", "-eo", "pipefail", "-c"] ]
      # installGhcDepsStep
      # fetchGhcStep (Ghc.Bindist.BindistSpec { version = ghcVersion, triple = "x86_64-alpine3_12-linux-static" })
      # Cabal.install (Cabal.Type.FromBindist "https://downloads.haskell.org/~ghcup/unofficial-bindists/cabal/3.6.2.0/cabal-install-3.6.2.0-x86_64-linux-alpine-static.tar.xz")
      # CF.run "update cabal index" [ "$CABAL update"]
      # HaskellTools.installGhcBuildDeps
      # createUserStep
      # [ CF.Statement.Cmd ["bash"] ]
    }
  , Image ::
    { name = "x86_64-linux-alpine3_17-wasm"
    , runnerTags = [ "x86_64-linux" ]
    , needs = [ "x86_64-linux-alpine3_17" ]
    , image =
        CF.from "${docker_base_url}/x86_64-linux-alpine3_17:latest"
      # [ CF.Statement.User "root" ]
      # [ CF.Statement.User "ghc"
        , CF.Statement.Cmd ["bash"]
        ]
      # installWasmDepsStep
    }
  , Image ::
    { name = "i386-linux-alpine3_12"
    , runnerTags = [ "x86_64-linux" ]
    , image =
        CF.from "i386/alpine:3.12.12"
      # [ CF.Statement.Shell ["/bin/ash", "-eo", "pipefail", "-c"] ]
      # installGhcDepsStep
      # fetchGhcStep (Ghc.Bindist.BindistURL "https://downloads.haskell.org/~ghcup/unofficial-bindists/ghc/9.0.1/ghc-9.0.1-i386-alpine-linux.tar.xz")
      # Cabal.install (Cabal.Type.FromBindist "https://downloads.haskell.org/~ghcup/unofficial-bindists/cabal/3.6.2.0/cabal-install-3.6.2.0-i386-linux-alpine.tar.xz")
      # CF.run "update cabal index" [ "$CABAL update"]
      # HaskellTools.installGhcBuildDeps
      # createUserStep
      # [ CF.Statement.Cmd ["bash"] ]
    }
  , Image ::
    { name = "x86_64-linux-alpine3_15"
    , runnerTags = [ "x86_64-linux" ]
    , image =
        CF.from "alpine:3.15.8"
      # [ CF.Statement.Shell ["/bin/ash", "-eo", "pipefail", "-c"] ]
      # installGhcDepsStep
      # installPkgs ["llvm12"]
      # fetchGhcStep (Ghc.Bindist.BindistSpec { version = ghcVersion, triple = "x86_64-alpine3_12-linux-static" })
      # Cabal.install (Cabal.Type.FromBindist "https://downloads.haskell.org/~ghcup/unofficial-bindists/cabal/3.6.2.0/cabal-install-3.6.2.0-x86_64-linux-alpine-static.tar.xz")
      # CF.run "update cabal index" [ "$CABAL update"]
      # HaskellTools.installGhcBuildDeps
      # createUserStep
      # [ CF.Statement.Cmd ["bash"] ]
    }
  , Image ::
    { name = "i386-linux-alpine3_15"
    , runnerTags = [ "x86_64-linux" ]
    , image =
        CF.from "i386/alpine:3.15.8"
      # [ CF.Statement.Shell ["/bin/ash", "-eo", "pipefail", "-c"] ]
      # installGhcDepsStep
      # installPkgs ["llvm12"]
      # fetchGhcStep (Ghc.Bindist.BindistURL "https://downloads.haskell.org/~ghcup/unofficial-bindists/ghc/9.0.1/ghc-9.0.1-i386-alpine-linux.tar.xz")
      # Cabal.install (Cabal.Type.FromBindist "https://downloads.haskell.org/~ghcup/unofficial-bindists/cabal/3.6.2.0/cabal-install-3.6.2.0-i386-linux-alpine.tar.xz")
      # CF.run "update cabal index" [ "$CABAL update"]
      # HaskellTools.installGhcBuildDeps
      # createUserStep
      # [ CF.Statement.Cmd ["bash"] ]
    }
  , Image ::
    { name = "x86_64-linux-alpine3_17"
    , runnerTags = [ "x86_64-linux" ]
    , image =
        CF.from "alpine:3.17.3"
      # [ CF.Statement.Shell ["/bin/ash", "-eo", "pipefail", "-c"] ]
      # installGhcDepsStep
      # installPkgs ["llvm15"]
      # fetchGhcStep (Ghc.Bindist.BindistSpec { version = ghcVersion, triple = "x86_64-alpine3_12-linux-static" })
      # Cabal.install (Cabal.Type.FromBindist "https://downloads.haskell.org/~ghcup/unofficial-bindists/cabal/3.6.2.0/cabal-install-3.6.2.0-x86_64-linux-alpine-static.tar.xz")
      # CF.run "update cabal index" [ "$CABAL update"]
      # HaskellTools.installGhcBuildDeps
      # createUserStep
      # [ CF.Statement.Cmd ["bash"] ]
    }
  , Image ::
    { name = "aarch64-linux-alpine3_18"
    , runnerTags = [ "aarch64-linux" ]
    , image =
        CF.from "arm64v8/alpine:3.18.0"
      # [ CF.Statement.Shell ["/bin/ash", "-eo", "pipefail", "-c"] ]
      # installGhcDepsStep
      # installPkgs ["ghc", "cabal"]
      # CF.env (toMap { GHC = "/usr/bin/ghc" })
      # CF.env (toMap { CABAL = "/usr/bin/cabal" })
      # CF.run "update cabal index" [ "$CABAL update"]
      # HaskellTools.installGhcBuildDeps
      # createUserStep
      # [ CF.Statement.Cmd ["bash"] ]
    }
  , Image ::
    { name = "i386-linux-alpine3_17"
    , runnerTags = [ "x86_64-linux" ]
    , image =
        CF.from "i386/alpine:3.17.3"
      # [ CF.Statement.Shell ["/bin/ash", "-eo", "pipefail", "-c"] ]
      # installGhcDepsStep
      # installPkgs ["llvm15"]
      # fetchGhcStep (Ghc.Bindist.BindistURL "https://downloads.haskell.org/~ghcup/unofficial-bindists/ghc/9.0.1/ghc-9.0.1-i386-alpine-linux.tar.xz")
      # Cabal.install (Cabal.Type.FromBindist "https://downloads.haskell.org/~ghcup/unofficial-bindists/cabal/3.6.2.0/cabal-install-3.6.2.0-i386-linux-alpine.tar.xz")
      # CF.run "update cabal index" [ "$CABAL update"]
      # HaskellTools.installGhcBuildDeps
      # createUserStep
      # [ CF.Statement.Cmd ["bash"] ]
    }
  ]

in images
