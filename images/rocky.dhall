-- Rocky Linux Docker images
let
  Prelude = ../deps/Prelude.dhall
let
  CF = ../deps/Containerfile.dhall
let
  Llvm = ../components/Llvm.dhall
let
  Ghc = ../components/Ghc.dhall
let
  HaskellTools = ../components/HaskellTools.dhall
let
  Cabal = ../components/Cabal.dhall
let
  nsswitchWorkaround = ../components/NsswitchWorkaround.dhall
let
  Image = ../Image.dhall

let
  coreBuildDepends: List Text =
    [ "binutils"
    , "which"
    , "git"
    , "make"
    , "automake"
    , "autoconf"
    , "gcc"
    , "perl"
    , "python3"
    , "texinfo"
    , "xz"
    , "bzip2"
    , "patch"
    , "openssh-clients"
    , "sudo"
    , "zlib-devel"
    , "sqlite"
    , "ncurses-compat-libs"
    , "gmp-devel"
    , "ncurses-devel"
    , "gcc-c++"
    , "findutils"
    , "diffutils"
    , "curl"
    , "wget"
    , "jq"
    -- For the dtrace code generator
    , "systemtap-sdt-devel"
    ]

let
  docBuildDepends: List Text =
    [ "python3-sphinx"
    , "texlive"
    , "texlive-latex"
    , "texlive-xetex"
    , "texlive-collection-latex"
    , "texlive-collection-latexrecommended"
    , "texlive-collection-xetex"
    , "python-sphinx-latex"
    , "dejavu-sans-fonts"
    , "dejavu-serif-fonts"
    , "dejavu-sans-mono-fonts"
    ]

let
  buildDepends: List Text = coreBuildDepends # docBuildDepends

-- systemd isn't running so remove it from nsswitch.conf
-- Failing to do this will result in testsuite failures due to
-- non-functional user lookup (#15230).
let
  nsswitchWorkaroundStep: CF.Type =
    CF.run "apply nsswitch workaround"
    [ "sed -i -e 's/systemd//g' /etc/nsswitch.conf" ]

let
  installDepsStep: CF.Type =
      CF.run "install build dependencies"
      [ "yum -y install ${Prelude.Text.concatSep " " buildDepends}" ]

-- Create a normal user
let
  createUserStep: CF.Type =
      CF.run "create user"
      [ "adduser ghc --comment 'GHC builds'"
      , "echo 'ghc ALL = NOPASSWD : ALL' > /etc/sudoers.d/ghc"
      ]
    # [CF.Statement.User "ghc"]
    # CF.workdir "/home/ghc/"
    # CF.run "update cabal index" [ "$CABAL update"]

let
  installLatestPython: CF.Type =
      CF.run "install latest python for testsuite"
      [
        "unset LLC OPT"
      , "cd $(mktemp -d)"
      , "curl -f -L --retry 5 https://www.python.org/ftp/python/3.11.2/Python-3.11.2.tar.xz | tar xJ --strip-components=1"
      , "./configure --prefix=/home/ghc/.local"
      , "make install -j$(curl -f -L --retry 5 https://gitlab.haskell.org/ghc/ghc/-/raw/master/mk/detect-cpu-count.sh | sh)"
      , "sudo rm -rf /tmp/*"
      ]
      # CF.env (toMap { PYTHON = "/home/ghc/.local/bin/python3" })

let
  RockyImage =
    let
      type: Type =
        { name : Text
        , fromImage : Text
        , runnerTags : List Text
        , bootstrapGhc : Ghc.BindistSpec
        , llvm : Optional Llvm.Source
        }
    let
      toDocker: type -> Image.Type = \(opts : type) ->
        let
          ghcDir: Text = "/opt/ghc/${opts.bootstrapGhc.version}"
        let
          llvmDir: Text = "/opt/llvm"

        let
          image: CF.Type =
          CF.from opts.fromImage
        # CF.env (toMap { LANG = "C.UTF-8" })
        # [ CF.Statement.Shell ["/bin/bash", "-o", "pipefail", "-c"] ]
        # CF.run "enable dnf config-manager plugin"
          [ "dnf install -y 'dnf-command(config-manager)'"
          , "dnf config-manager --set-enabled powertools"
          ]
        # installDepsStep

        # nsswitchWorkaroundStep

          -- install GHC
        # Ghc.install
            { bindist = Ghc.Bindist.BindistSpec opts.bootstrapGhc
            , destDir = ghcDir
            , configureOpts = [] : List Text
            }
        # CF.env (toMap { GHC = "${ghcDir}/bin/ghc" })

          -- install LLVM to be used by built compiler
        # Llvm.maybeInstallTo llvmDir opts.llvm

          -- install cabal-install
        # Cabal.install (Cabal.fromUpstreamBindist { version = "3.6.2.0", triple = "x86_64-linux-alpine-static" })

          -- install hscolour, alex, and happy
        # CF.run "update cabal index" [ "$CABAL update"]
        # HaskellTools.installGhcBuildDeps

        # nsswitchWorkaround
        # createUserStep
        # installLatestPython
        # [ CF.Statement.Cmd ["bash"] ]

        in
        Image::
        { name = opts.name
        , runnerTags = opts.runnerTags
        , image = image
        }
    in
    { Type = type
    , toDocker = toDocker
    }

let images: List Image.Type =
[ RockyImage.toDocker
    { name = "x86_64-linux-rocky8"
    , fromImage = "amd64/rockylinux:8"
    , runnerTags = [ "x86_64-linux" ]
    , bootstrapGhc = { version = "9.4.5", triple = "x86_64-fedora27-linux" }
    , llvm = Some (Llvm.Source.FromBindist { version = "12.0.1" , triple = "x86_64-linux-gnu-ubuntu-16.04" })
    }
]
in images
