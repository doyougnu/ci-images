-- Debian/Ubuntu Linux Docker images

let
  Prelude = ../deps/Prelude.dhall
let
  CF = ../deps/Containerfile.dhall
let
  Llvm = ../components/Llvm.dhall
let
  Ghc = ../components/Ghc.dhall
let
  HaskellTools = ../components/HaskellTools.dhall
let
  Cabal = ../components/Cabal.dhall
let
  Image = ../Image.dhall

let
  docker_base_url: Text = "registry.gitlab.haskell.org/ghc/ci-images"

let
  coreBuildDepends: List Text =
    [ "zlib1g-dev"
    , "libtinfo-dev"
    , "libsqlite3-0"
    , "libsqlite3-dev"
    , "libgmp-dev"
    , "libncurses-dev"
    , "ca-certificates"
    , "g++"
    , "git"
    , "make"
    , "automake"
    , "autoconf"
    , "gcc"
    , "perl"
    , "python3"
    , "texinfo"
    , "xz-utils"
    , "lbzip2"
    , "bzip2"
    , "patch"
    , "openssh-client"
    , "sudo"
    , "time"
    , "jq"
    , "wget"
    , "curl"
    , "locales"
    -- For LLVM
    , "libtinfo5"
    -- DWARF libraries
    , "libdw1", "libdw-dev"
    -- For nofib
    , "valgrind"
    -- For the dtrace code generator and headers
    , "systemtap-sdt-dev"
    -- For lndir
    , "xutils-dev"
    -- For wasm CI jobs
    , "unzip"
    -- For python
    , "pkg-config"
    ]

let
  docsBuildDepends: List Text =
    [ "python3-sphinx"
    , "texlive-xetex"
    , "texlive-latex-extra"
    , "texlive-binaries"
    , "texlive-fonts-recommended"
    , "lmodern"
    , "tex-gyre"
    ]

let debianBuildDepends: List Text =
    [ "texlive-generic-extra" ]

let ubuntuBuildDepends: List Text =
    [ "texlive-plain-generic" ]

let
  buildDepends: List Text = coreBuildDepends # docsBuildDepends

let
  RepoType = < Standard | Archive >

let
  updateRepositories: RepoType -> CF.Type =
    \(repo : RepoType) ->
      let useArchives : CF.Type =
        [ CF.Statement.Comment "Use archive repos"
        , CF.Statement.Exec [ "sed", "-i", "s/deb.debian.org/archive.debian.org/g", "/etc/apt/sources.list" ]
        , CF.Statement.Exec [ "sed", "-i", "s|security.debian.org|archive.debian.org/|g", "/etc/apt/sources.list" ]
        , CF.Statement.Exec [ "sed", "-i", "/-updates/d", "/etc/apt/sources.list" ]
        , CF.Statement.Empty
        ]

      in merge { Standard = [] : CF.Type, Archive = useArchives } repo
let
  installPackages: List Text -> CF.Type =
    \(pkgs: List Text) ->
        CF.arg (toMap { DEBIAN_FRONTEND = "noninteractive" })
      # CF.run "install build dependencies"
        [ "apt-get update"
        , "apt-get install --no-install-recommends -qy ${Prelude.Text.concatSep " " pkgs}"
        ]

let
  cleanApt: CF.Type =
      CF.run "clean apt cache"
      [ "apt-get clean"
      , "rm -rf /var/lib/apt/lists/*"
      ]

-- Create a normal user
let
  createUserStep: CF.Type =
      CF.run "create user"
      [ "adduser ghc --gecos 'GHC builds' --disabled-password"
      , "echo 'ghc ALL = NOPASSWD : ALL' > /etc/sudoers.d/ghc"
      ]
    # [CF.Statement.User "ghc"]
    # CF.workdir "/home/ghc/"
    # CF.run "update cabal index" [ "$CABAL update"]

-- Install stack for testing hadrian
let
  installStack: CF.Type =
      CF.run "install stack"
      [ "curl -sSL https://get.haskellstack.org/ | sh"
      ]

let
  installEMSDK: CF.Type =
      CF.run "install emsdk"
      [ "sudo mkdir /emsdk"
      , "sudo chown ghc:ghc /emsdk"
      , "curl -f -L --retry 5 https://github.com/emscripten-core/emsdk/archive/refs/tags/3.1.40.tar.gz | tar xz --strip-components=1 -C /emsdk"
      , "/emsdk/emsdk install latest"
      , "/emsdk/emsdk activate latest"
      ]
      # CF.env (toMap { EMSDK = "/emsdk" })

let
  installNodeJS: CF.Type =
      CF.run "install nodejs"
      [ "curl -fsSL https://deb.nodesource.com/setup_21.x | sudo -E bash - && sudo apt-get install -y nodejs"
      ]

let
  installLatestPython: CF.Type =
      CF.run "install latest python for testsuite"
      [
        "unset LLC OPT"
      , "cd $(mktemp -d)"
      , "curl -f -L --retry 5 https://www.python.org/ftp/python/3.11.2/Python-3.11.2.tar.xz | tar xJ --strip-components=1"
      , "./configure --build=$(dpkg --print-architecture)-linux --prefix=/home/ghc/.local"
      , "make install -j$(curl -f -L --retry 5 https://gitlab.haskell.org/ghc/ghc/-/raw/master/mk/detect-cpu-count.sh | sh)"
      , "sudo rm -rf /tmp/*"
      ]
      # CF.env (toMap { PYTHON = "/home/ghc/.local/bin/python3" })

let
  installLibzstd: CF.Type =
      CF.run "install libzstd >= 1.4.0, required for IPE data compression"
      [ "sudo mkdir /libzstd"
      , "sudo chown ghc:ghc /libzstd"
      , "curl -f -L --retry 5 https://github.com/facebook/zstd/releases/download/v1.5.5/zstd-1.5.5.tar.gz | tar xz --strip-components=1 -C /libzstd"
      , "cd /libzstd/lib"
      , "sudo make prefix=/usr install -j$(curl -f -L --retry 5 https://gitlab.haskell.org/ghc/ghc/-/raw/master/mk/detect-cpu-count.sh | sh)"
      ]

let
  DebianImage =
    let
      type: Type =
        { name: Text
        , fromImage : Text
        , runnerTags : List Text
        , bootstrapLlvm : Optional Llvm.Source
        , bootstrapGhc : Ghc.BindistSpec
        , llvm : Optional Llvm.Source
        , cabalSource : Cabal.Type
        , extraPackages: List Text
          -- N.B. Optional as Stack's installer does not support all platforms
        , withStack : Bool
        , withEMSDK : Bool
        -- deb9/ubuntu18 python too old for testsuite driver, needs at least 3.7
        , withLatestPython : Bool
        , withLibzstd : Bool
        , repoType : RepoType
        }

    let
      toDocker: type -> Image.Type = \(opts : type) ->
        let
          ghcDir: Text = "/opt/ghc/${opts.bootstrapGhc.version}"
        let
          bootLlvmDir: Text = "/opt/llvm-bootstrap"
        let
          llvmDir: Text = "/opt/llvm"
        let
          bootstrapLlvmConfigureOptions =
            merge { Some = \(_: Llvm.Source) -> [ "LLC=${bootLlvmDir}/bin/llc", "OPT=${bootLlvmDir}/bin/opt" ]
                  , None = [] : List Text
                  } opts.bootstrapLlvm

        let
          image =
            CF.from opts.fromImage
          # CF.env (toMap { LANG = "C.UTF-8" })
          # [ CF.Statement.Shell ["/bin/bash", "-o", "pipefail", "-c"] ]
          # updateRepositories opts.repoType
          # installPackages (buildDepends # opts.extraPackages)
          # (if opts.withStack then installStack else [] : CF.Type)
          # cleanApt

            -- install LLVM for bootstrap GHC
          # Llvm.maybeInstallTo bootLlvmDir opts.bootstrapLlvm

            -- install GHC
          # Ghc.install
              { bindist = Ghc.Bindist.BindistSpec opts.bootstrapGhc
              , destDir = ghcDir
              , configureOpts = bootstrapLlvmConfigureOptions
              }
          # CF.env (toMap { GHC = "${ghcDir}/bin/ghc" })

            -- install LLVM to be used by built compiler
          # Llvm.maybeInstallTo llvmDir opts.llvm
          # Llvm.setEnv llvmDir

            -- install cabal-install
          # Cabal.install opts.cabalSource

            -- install hscolour, alex, and happy
          # CF.run "update cabal index" [ "$CABAL update"]
          # HaskellTools.installGhcBuildDeps

          # createUserStep
          # (if opts.withEMSDK then installEMSDK # installNodeJS else [] : CF.Type)
          # (if opts.withLatestPython then installLatestPython else [] : CF.Type)
          # (if opts.withLibzstd then installLibzstd else [] : CF.Type)
          # [ CF.Statement.Cmd ["bash"] ]

        in Image::{ name = opts.name, runnerTags = opts.runnerTags, image = image }
    in
    { Type = type
    , default =
      { withStack = True
      , withEMSDK = False
      , withLatestPython = False
      , withLibzstd = False
      , repoType = RepoType.Standard
      }
    , toDocker = toDocker
    }

let debian11Images: List Image.Type =
[ DebianImage.toDocker DebianImage::
    { name = "x86_64-linux-deb11"
    , fromImage = "amd64/debian:bullseye"
    , runnerTags = [ "x86_64-linux" ]
    , bootstrapLlvm = Some (Llvm.Source.FromBindist { version = "12.0.1", triple = "x86_64-linux-gnu-ubuntu-16.04" })
    , bootstrapGhc = { version = "9.4.3", triple = "x86_64-deb10-linux" }
    , llvm = Some (Llvm.Source.FromBindist { version = "12.0.1" , triple = "x86_64-linux-gnu-ubuntu-16.04" })
    , cabalSource = Cabal.fromUpstreamBindist { version = "3.6.2.0", triple = "x86_64-linux-alpine-static" }
    , extraPackages =
          ubuntuBuildDepends
        -- For cross-compilation testing
        # [ "crossbuild-essential-arm64", "crossbuild-essential-s390x", "qemu-user" ]
        # [ "libnuma-dev" ]
    , withEMSDK = True
    }

, DebianImage.toDocker DebianImage::
    { name = "armv7-linux-deb11"
    , fromImage = "arm32v7/debian:bullseye"
    , runnerTags = [ "armv7-linux" ]
    , bootstrapLlvm = Some (Llvm.Source.FromBindist { version = "12.0.1", triple = "armv7a-linux-gnueabihf" })
    , bootstrapGhc = { version = "9.4.3", triple = "armv7-deb10-linux" }
    , llvm = Some (Llvm.Source.FromBindist { version = "12.0.1" , triple = "armv7a-linux-gnueabihf" })
    , cabalSource = (Cabal.Type.FromBindist "https://downloads.haskell.org/~ghcup/unofficial-bindists/cabal/3.6.2.0/cabal-install-3.6.2.0-armv7-linux-deb10.tar.xz")
    , extraPackages = [ "libnuma-dev" ] # ubuntuBuildDepends
    , withStack = False
    }
, DebianImage.toDocker DebianImage::
    { name = "aarch64-linux-deb11"
    , fromImage = "arm64v8/debian:bullseye"
    , runnerTags = [ "aarch64-linux" ]
    , bootstrapLlvm = Some (Llvm.Source.FromBindist { version = "12.0.1" , triple = "aarch64-linux-gnu" })
    , bootstrapGhc = { version = "9.4.3", triple = "aarch64-deb10-linux" }
    , llvm = Some (Llvm.Source.FromBindist { version = "12.0.1" , triple = "aarch64-linux-gnu" })
    , cabalSource = Cabal.fromUpstreamBindist { version = "3.6.2.0", triple = "aarch64-linux-deb10" }
    , extraPackages = [ "libnuma-dev" ] : List Text
    , withStack = False
    }
]

let debian12Images: List Image.Type =
[ DebianImage.toDocker DebianImage::
    { name = "x86_64-linux-deb12"
    , fromImage = "amd64/debian:bookworm"
    , runnerTags = [ "x86_64-linux" ]
    , bootstrapLlvm = Some (Llvm.Source.FromBindist { version = "15.0.6", triple = "x86_64-linux-gnu-ubuntu-18.04" })
    , bootstrapGhc = { version = "9.4.3", triple = "x86_64-deb10-linux" }
    , llvm = Some (Llvm.Source.FromBindist { version = "15.0.6" , triple = "x86_64-linux-gnu-ubuntu-18.04" })
    , cabalSource = Cabal.fromUpstreamBindist { version = "3.6.2.0", triple = "x86_64-linux-alpine-static" }
    , extraPackages =
          ubuntuBuildDepends
        -- For cross-compilation testing
        # [ "crossbuild-essential-arm64", "crossbuild-essential-s390x", "qemu-user" ]
        # [ "libnuma-dev" ]
    , withEMSDK = False
    }

, DebianImage.toDocker DebianImage::
    { name = "aarch64-linux-deb12"
    , fromImage = "arm64v8/debian:bookworm"
    , runnerTags = [ "aarch64-linux" ]
    , bootstrapLlvm = Some (Llvm.Source.FromBindist { version = "15.0.6" , triple = "aarch64-linux-gnu" })
    , bootstrapGhc = { version = "9.4.3", triple = "aarch64-deb10-linux" }
    , llvm = Some (Llvm.Source.FromBindist { version = "15.0.6" , triple = "aarch64-linux-gnu" })
    , cabalSource = Cabal.fromUpstreamBindist { version = "3.6.2.0", triple = "aarch64-linux-deb10" }
    , extraPackages = [ "libnuma-dev" ] : List Text
    , withStack = False
    }
]

let debian10Images: List Image.Type =
[ DebianImage.toDocker DebianImage::
    { name = "aarch64-linux-deb10"
    , fromImage = "arm64v8/debian:buster"
    , runnerTags = [ "aarch64-linux" ]
    , bootstrapLlvm = Some (Llvm.Source.FromBindist { version = "12.0.1" , triple = "aarch64-linux-gnu" })
    , bootstrapGhc = { version = "9.4.3", triple = "aarch64-deb10-linux" }
    , llvm = Some (Llvm.Source.FromBindist { version = "12.0.1" , triple = "aarch64-linux-gnu" })
    , cabalSource = Cabal.fromUpstreamBindist { version = "3.6.2.0", triple = "aarch64-linux-deb10" }
    , extraPackages = [ "libnuma-dev" ] : List Text
    , withStack = False
    }
, DebianImage.toDocker DebianImage::
    { name = "armv7-linux-deb10"
    , fromImage = "arm32v7/debian:buster"
    , runnerTags = [ "armv7-linux" ]
    , bootstrapLlvm = Some (Llvm.Source.FromBindist { version = "12.0.1", triple = "armv7a-linux-gnueabihf" })
    , bootstrapGhc = { version = "9.4.3", triple = "armv7-deb10-linux" }
    , llvm = Some (Llvm.Source.FromBindist { version = "12.0.1" , triple = "armv7a-linux-gnueabihf" })
    , cabalSource = (Cabal.Type.FromBindist "https://downloads.haskell.org/~ghcup/unofficial-bindists/cabal/3.6.2.0/cabal-install-3.6.2.0-armv7-linux-deb10.tar.xz")
    , extraPackages = [ "libnuma-dev", "lld" ] # debianBuildDepends
    , withStack = False
    }
    -- N.B. Need GHC bindist for deb10 i386
, DebianImage.toDocker DebianImage::
    { name = "i386-linux-deb10"
    , fromImage = "i386/debian:buster"
    , runnerTags = [ "x86_64-linux" ]
    , bootstrapLlvm = None Llvm.Source
    , bootstrapGhc = { version = "9.4.3", triple = "i386-deb9-linux" }
    , llvm = None Llvm.Source
    , cabalSource = Cabal.fromUpstreamBindist { version = "3.6.2.0", triple = "x86_64-linux-alpine-static" }
    , extraPackages =
        debianBuildDepends
      # [ "libnuma-dev"   ]
      # [ "cabal-install" ]
    , withStack = False
    }
, DebianImage.toDocker DebianImage::
    { name = "x86_64-linux-deb10"
    , fromImage = "amd64/debian:buster"
    , runnerTags = [ "x86_64-linux" ]
    , bootstrapLlvm = Some (Llvm.Source.FromBindist { version = "12.0.1", triple = "x86_64-linux-gnu-ubuntu-16.04" })
    , bootstrapGhc = { version = "9.4.3", triple = "x86_64-deb10-linux" }
    , llvm = Some (Llvm.Source.FromBindist { version = "12.0.1" , triple = "x86_64-linux-gnu-ubuntu-16.04" })
    , cabalSource = Cabal.fromUpstreamBindist { version = "3.6.2.0", triple = "x86_64-linux-alpine-static" }
    , extraPackages =
          debianBuildDepends
        -- For cross-compilation testing
        # [ "crossbuild-essential-arm64", "qemu-user" ]
        # [ "libnuma-dev" ]
    , withLibzstd = True
    }
]

let debian9Images: List Image.Type =
[ DebianImage.toDocker DebianImage::
    { name = "x86_64-linux-deb9"
    , fromImage = "amd64/debian:stretch"
    , runnerTags = [ "x86_64-linux" ]
    , bootstrapLlvm = None Llvm.Source
    , bootstrapGhc = { version = "9.4.3", triple = "x86_64-deb9-linux" }
    , llvm = Some (Llvm.Source.FromBindist { version = "12.0.1" , triple = "x86_64-linux-gnu-ubuntu-16.04" })
    , cabalSource = Cabal.fromUpstreamBindist { version = "3.6.2.0", triple = "x86_64-linux-alpine-static" }
    , extraPackages = debianBuildDepends : List Text
    , withLatestPython = True
    , repoType = RepoType.Archive
    }
, DebianImage.toDocker DebianImage::
    { name = "i386-linux-deb9"
    , fromImage = "i386/debian:stretch"
    , runnerTags = [ "x86_64-linux" ]
    , bootstrapLlvm = None Llvm.Source
    , bootstrapGhc = { version = "9.4.3", triple = "i386-deb9-linux" }
    , llvm = None Llvm.Source
    , cabalSource = (Cabal.Type.FromBindist "https://downloads.haskell.org/~ghcup/unofficial-bindists/cabal/3.6.2.0/cabal-install-3.6.2.0-i386-linux-alpine.tar.xz")
    , extraPackages = debianBuildDepends # [ "cabal-install" ] : List Text
    , withStack = False
    , withLatestPython = True
    , repoType = RepoType.Archive
    }
]

let ubuntuImages: List Image.Type =
[
  DebianImage.toDocker DebianImage::
    { name = "x86_64-linux-ubuntu22_04"
    , fromImage = "amd64/ubuntu:22.04"
    , runnerTags = [ "x86_64-linux" ]
    , bootstrapLlvm = None Llvm.Source
    , bootstrapGhc = { version = "9.4.3", triple = "x86_64-deb10-linux" }
    , llvm = Some (Llvm.Source.FromBindist { version = "15.0.6" , triple = "x86_64-linux-gnu-ubuntu-18.04" })
    , cabalSource = Cabal.fromUpstreamBindist { version = "3.6.2.0", triple = "x86_64-linux-alpine-static" }
    , extraPackages = ubuntuBuildDepends
    }
  , DebianImage.toDocker DebianImage::
    { name = "x86_64-linux-ubuntu20_04"
    , fromImage = "amd64/ubuntu:20.04"
    , runnerTags = [ "x86_64-linux" ]
    , bootstrapLlvm = None Llvm.Source
    , bootstrapGhc = { version = "9.4.3", triple = "x86_64-deb9-linux" }
    , llvm = Some (Llvm.Source.FromBindist { version = "12.0.1" , triple = "x86_64-linux-gnu-ubuntu-16.04" })
    , cabalSource = Cabal.fromUpstreamBindist { version = "3.6.2.0", triple = "x86_64-linux-alpine-static" }
    , extraPackages = ubuntuBuildDepends
    }
  , DebianImage.toDocker DebianImage::
    { name = "x86_64-linux-ubuntu18_04"
    , fromImage = "amd64/ubuntu:18.04"
    , runnerTags = [ "x86_64-linux" ]
    , bootstrapLlvm = None Llvm.Source
    , bootstrapGhc = { version = "9.4.3", triple = "x86_64-deb9-linux" }
    , llvm = Some (Llvm.Source.FromBindist { version = "11.0.1" , triple = "x86_64-linux-gnu-ubuntu-16.04" })
    , cabalSource = Cabal.fromUpstreamBindist { version = "3.6.2.0", triple = "x86_64-linux-alpine-static" }
    , extraPackages = ubuntuBuildDepends
    , withLatestPython = True
    }
]

let linterImages: List Image.Type =
[ Image::
  { name = "linters"
  , runnerTags = [ "x86_64-linux" ]
  , jobStage = "build-derived"
  , needs = [ "x86_64-linux-deb10" ]
  , image =
      let installMypy: CF.Type =
          installPackages [ "python3-setuptools", "python3-pip", "python3-dev" ]
        # CF.run "installing wheel" [ "pip3 install wheel" ]
        # CF.run "installing mypy" [ "pip3 install mypy==1.0" ]

      let lintersCommit: Text = "b376b0c20e033e71751fe2059daba1ba40b886be"
      let installShellcheck: CF.Type =
          installPackages [ "shellcheck" ]
      in
          CF.from "${docker_base_url}/x86_64-linux-deb10:latest"
        # [ CF.Statement.User "root" ]
        # installMypy
        # installShellcheck
        # HaskellTools.installHLint
        # [CF.Statement.User "ghc"]
  }
]

let bootstrapImages: List Image.Type =
[ DebianImage.toDocker DebianImage::
    { name = "x86_64-linux-deb10-ghc9_2"
    , fromImage = "amd64/debian:buster"
    , runnerTags = [ "x86_64-linux" ]
    , bootstrapLlvm = Some (Llvm.Source.FromBindist { version = "12.0.1", triple = "x86_64-linux-gnu-ubuntu-16.04" })
    , bootstrapGhc = { version = "9.2.8", triple = "x86_64-deb10-linux" }
    , llvm = Some (Llvm.Source.FromBindist { version = "12.0.1" , triple = "x86_64-linux-gnu-ubuntu-16.04" })
    , cabalSource = Cabal.fromUpstreamBindist { version = "3.6.2.0", triple = "x86_64-linux-alpine-static" }
    , extraPackages =
          debianBuildDepends
        -- For cross-compilation testing
        # [ "crossbuild-essential-arm64", "qemu-user" ]
        # [ "libnuma-dev" ]
    }
, DebianImage.toDocker DebianImage::
    { name = "x86_64-linux-deb10-ghc9_4"
    , fromImage = "amd64/debian:buster"
    , runnerTags = [ "x86_64-linux" ]
    , bootstrapLlvm = Some (Llvm.Source.FromBindist { version = "12.0.1", triple = "x86_64-linux-gnu-ubuntu-16.04" })
    , bootstrapGhc = { version = "9.4.7", triple = "x86_64-deb10-linux" }
    , llvm = Some (Llvm.Source.FromBindist { version = "12.0.1" , triple = "x86_64-linux-gnu-ubuntu-16.04" })
    , cabalSource = Cabal.fromUpstreamBindist { version = "3.6.2.0", triple = "x86_64-linux-alpine-static" }
    , extraPackages =
          debianBuildDepends
        -- For cross-compilation testing
        # [ "crossbuild-essential-arm64", "qemu-user" ]
        # [ "libnuma-dev" ]
    }
, DebianImage.toDocker DebianImage::
    { name = "x86_64-linux-deb10-ghc9_6"
    , fromImage = "amd64/debian:buster"
    , runnerTags = [ "x86_64-linux" ]
    , bootstrapLlvm = Some (Llvm.Source.FromBindist { version = "12.0.1", triple = "x86_64-linux-gnu-ubuntu-16.04" })
    , bootstrapGhc = { version = "9.6.3", triple = "x86_64-deb10-linux" }
    , llvm = Some (Llvm.Source.FromBindist { version = "12.0.1" , triple = "x86_64-linux-gnu-ubuntu-16.04" })
    , cabalSource = Cabal.fromUpstreamBindist { version = "3.10.1.0", triple = "x86_64-linux-deb10" }
    , extraPackages =
          debianBuildDepends
        -- For cross-compilation testing
        # [ "crossbuild-essential-arm64", "qemu-user" ]
        # [ "libnuma-dev" ]
    }
, DebianImage.toDocker DebianImage::
    { name = "x86_64-linux-deb10-ghc9_8"
    , fromImage = "amd64/debian:buster"
    , runnerTags = [ "x86_64-linux" ]
    , bootstrapLlvm = Some (Llvm.Source.FromBindist { version = "12.0.1", triple = "x86_64-linux-gnu-ubuntu-16.04" })
    , bootstrapGhc = { version = "9.8.1", triple = "x86_64-deb10-linux" }
    , llvm = Some (Llvm.Source.FromBindist { version = "12.0.1" , triple = "x86_64-linux-gnu-ubuntu-16.04" })
    , cabalSource = Cabal.fromUpstreamBindist { version = "3.10.1.0", triple = "x86_64-linux-deb10" }
    , extraPackages =
          debianBuildDepends
        -- For cross-compilation testing
        # [ "crossbuild-essential-arm64", "qemu-user" ]
        # [ "libnuma-dev" ]
    }
]

let allImages: List Image.Type =
  linterImages # debian12Images # debian11Images # debian10Images # debian9Images # ubuntuImages # bootstrapImages

in allImages
